
Foram criados dois projetos: credito-front-end / credito-service , sendo front-end em Angular e back-end em Java/Springboot/Persistencia
Partindo da premissia que o ambiente Node,NPM,Git, Maven e Java 8 estão instalados e funcionando corretamente.

1. Abra o terminal
	2. clone o projeto com o comando: git clone https://bvalentim@bitbucket.org/bvalentim/ahoy-teste.git
	
Back-End
	1. Abra o terminal
	2. Entre no diretório credito-service, execute o comando maven para baixar,instalar e compilar junto com as dependências:
		mvn clean install
	3. Execute o seguinte comando para iniciar o back-end :
		 java -jar target/credito-service-0.0.1-SNAPSHOT.jar 

Front-End
	1. Abra o terminal
	3. Entre no diretório credito-front-end,execute o comando:
		ng serve ( ou ng s)

Acessa do seu navegador preferido a seguinte URL: 
	http://localhost:4200/
