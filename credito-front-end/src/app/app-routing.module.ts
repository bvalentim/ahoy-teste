import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientesComponent } from './clientes/clientes.component';
import { AddClienteComponent } from './add-cliente/add-cliente.component';

const routes: Routes = [
  { path: 'clientes', component: ClientesComponent },
  { path: 'add', component: AddClienteComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
