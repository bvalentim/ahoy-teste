import { Component, OnInit } from '@angular/core';
import { Cliente } from '../models/cliente.model';
import { ClienteService } from '../services/cliente.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientesDataSource: any[] = new Array();
  riscosDataSource: string[] = ['A','B','C'];

  constructor(private clienteService: ClienteService) { }

  ngOnInit() {
    this.clienteService.getClientes()
      .subscribe(data => {
            this.clientesDataSource = data;
       });
  }

  deleteCliente(cliente:Cliente):void{
    this.clienteService.deleteCliente(cliente)
      .subscribe(data => {
          this.clientesDataSource = this.clientesDataSource.filter(c => c != cliente);
      });
  }


}
