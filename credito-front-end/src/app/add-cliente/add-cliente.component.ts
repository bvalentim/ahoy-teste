import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../services/cliente.service';
import { Cliente } from '../models/cliente.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-cliente',
  templateUrl: './add-cliente.component.html',
  styleUrls: ['./add-cliente.component.css']
})
export class AddClienteComponent implements OnInit {

  cliente:Cliente = new Cliente();
  constructor(private router: Router, private clienteService: ClienteService) { }

  ngOnInit() {
  }

  createCliente(): void {
      if(this.validateInputs(this.cliente)){
        this.clienteService.createCliente(this.cliente)
            .subscribe( data => {
              alert("Cliente criado com sucesso!.");
              this.clear(this.cliente);
            });
      }else{
        alert("Todos os campos são obrigatórios!.");
      }

    };

  clear(cliente: Cliente) :void{
      cliente.nome = '';
      cliente.limiteCredito = 0;
      cliente.risco = 'A';
      cliente.taxaJuros = 0;
  }

  validateInputs(cliente: Cliente): boolean{
    return cliente.nome != ''
          && cliente.limiteCredito > 0
          && cliente.risco != '';
  }

  onChangeRisco():void {
    let inputRisco: HTMLInputElement = document.getElementById('risco') as HTMLInputElement;
    let inputTaxaJuros: HTMLInputElement = document.getElementById('taxaJuros') as HTMLInputElement;

    this.cliente.risco = inputRisco.value;

    if(inputRisco.value == 'B'){
      inputTaxaJuros.value = 10.0;
      this.cliente.taxaJuros = inputTaxaJuros.value;
    }else if(inputRisco.value == 'C'){
      inputTaxaJuros.value = 20.0;
      this.cliente.taxaJuros = inputTaxaJuros.value;
    }
  }

}
