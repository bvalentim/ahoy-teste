export class Cliente {

  id: number;
  nome: string;
  limiteCredito: number;
  taxaJuros: number;
  risco: string;
}
