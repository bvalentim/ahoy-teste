import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Cliente } from '../models/cliente.model';


const httpOptions = {
headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ClienteService {

  constructor(private http:HttpClient) {}


	private clienteUrl = 'http://localhost:8080/api/clientes/';

  public getClientes() {
    return this.http.get<Cliente[]>(this.clienteUrl);
  }

  public deleteCliente(cliente) {
    return this.http.delete(this.clienteUrl + "/"+ cliente.id);
  }

  public createCliente(cliente) {
    return this.http.post<Cliente>(this.clienteUrl, cliente);
  }

}
