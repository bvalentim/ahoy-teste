package br.com.ahoy.test.brunovalentim.creditowebapp.services;

import br.com.ahoy.test.brunovalentim.creditowebapp.models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.ahoy.test.brunovalentim.creditowebapp.repositories.ClienteRepository;

import java.util.List;

@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Override
    public Cliente create(Cliente cliente) {
        return repository.save(cliente);
    }

    @Override
    public Cliente delete(long id) {
        Cliente cliente = findById(id);
        if(cliente != null){
            repository.delete(cliente);
        }
        return cliente;
    }

    @Override
    public List findAll() {
        return repository.findAll();
    }

    @Override
    public Cliente findById(long id) {
        return repository.findById(id);
    }

    @Override
    public Cliente update(Cliente cliente) {
        return null;
    }
}
