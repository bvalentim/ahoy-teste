package br.com.ahoy.test.brunovalentim.creditowebapp.repositories;

import br.com.ahoy.test.brunovalentim.creditowebapp.models.Cliente;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface ClienteRepository extends Repository<Cliente,Long> {

    void delete(Cliente cliente);

    List findAll();

    Cliente findById(long id);

    Cliente save(Cliente cliente);
}
