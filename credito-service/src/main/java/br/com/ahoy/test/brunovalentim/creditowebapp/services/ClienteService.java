package br.com.ahoy.test.brunovalentim.creditowebapp.services;

import br.com.ahoy.test.brunovalentim.creditowebapp.models.Cliente;

import java.util.List;

public interface ClienteService {
    Cliente create(Cliente cliente);

    Cliente delete(long id);

    List findAll();

    Cliente findById(long id);

    Cliente update(Cliente cliente);
}
