package br.com.ahoy.test.brunovalentim.creditowebapp.controllers;

import br.com.ahoy.test.brunovalentim.creditowebapp.models.Cliente;
import br.com.ahoy.test.brunovalentim.creditowebapp.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController()
@RequestMapping({"/api/clientes"})
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public Cliente create(@RequestBody Cliente cliente){
        return clienteService.create(cliente);
    }

    @GetMapping(path = {"/{id}"})
    public Cliente find(@PathVariable("id") long id){
        return clienteService.findById(id);
    }

    @PutMapping
    public Cliente update(@RequestBody Cliente cliente){
        return clienteService.update(cliente);
    }

    @DeleteMapping(path ={"/{id}"})
    public Cliente delete(@PathVariable("id") int id) {
        return clienteService.delete(id);
    }

    @GetMapping
    public List findAll(){
        return clienteService.findAll();
    }
}
